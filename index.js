
// let is used to set a scoped variable,
// let variable définit de manière local. Ne peut-être utilisé dans un autre dossier.

const primitiveAffectation = () => {
    let lastname = "Remi"
    return lastname
};

let birtdata = new Date(1999, 12, 3)
let myBirthData = birtdata

console.log(birtdata)
birtdata.setMonth(5)
console.log(myBirthData)

// Retourne une copie d'objet en destructurant l'objet
const objectCopy = (aDate) => {
    return { ...aDate }
}

const usingPrototypePattern = (aDate) => {

    if (aDate instanceof Date) {

        const anonymousDate = new Date()
        anonymousDate.setDate(aDate.getDate())
        anonymousDate.setMonth(aDate.getMonth())
        anonymousDate.setFullYear(aDate.getFullYear())

        return anonymousDate
    } 
        throw new TypeError("'aDate' was different what expect.")
}

const usingArrays = () => 
{
    const myArray = [1, 2, 3, 5, 8, 13, 21, 34]
    myArray.push(55)
    myArray.pop()

    return myArray
}

const objectArray = [
    {
        "id": "1fe34",
        "name": "Bananes",
        "stock": 12
    },
    {
        "id": "4ae3",
        "name": "Café en grain",
        "stock": 6
    },
    {
        "id": "3cc52",
        "name": "Raviolis",
        "stock": 3
    },

]

module.exports = {
    primitiveAffectation,
    birtdata,
    myBirthData,
    objectCopy,
    usingPrototypePattern,
    usingArrays,
    objectArray,
}
