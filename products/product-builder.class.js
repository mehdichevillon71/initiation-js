import { Builder } from "../_helpers/builder.class";
import { Product } from "./product.class";
import { NameIsEmptyError } from "../errors/name-is-empty.error";
import { IdIsEmptyError } from "../errors/id-is-empty.error";
import { StockNegativeError } from "../errors/stock-negative.error";
import { StockNullError } from "../errors/stock-not-valid.error";

export class ProductBuilder extends Builder {
    /** 
    * ID of the product
    * @var string
    */

    id = ''

    /** 
    * Name of the product
    * @var string
    */

    name = ''

    /** 
    * Stock of the product
    * @var number
    */

    stock = 0
    /**
     * Build a concrete Product
     * Throws exceptions
     * @returns Product
     * @see Builder
     * @override
     */

    build() {

        if (this.id === '') {
            throw new IdIsEmptyError()
        }
        if (this.name === '') {
            throw new NameIsEmptyError()
        }
        if (isNaN(this.stock)) {
            throw new StockNullError()
        }
        if(this.stock < 0)
        {
            throw new StockNegativeError()
        }

        const product = Product.getInstance()
        product.id = this.id
        product.name = this.name
        product.stock = this.stock

        return product
    }
}