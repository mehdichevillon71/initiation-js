export class Builder {
    build() {
        throw new Error('Cannot invoke this method, please implement in children classes')
    }
}