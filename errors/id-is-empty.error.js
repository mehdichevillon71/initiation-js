export class IdIsEmptyError extends Error
{
    constructor() {
        super("Cannot build Product without ID")
    }
}