export class NameIsEmptyError extends Error
{
    constructor() {
        super("Cannot Build without correct Name")
    }
}