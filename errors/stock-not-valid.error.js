export class StockNullError extends Error
{
    constructor() {
        super("Cannot Build Null path")
    }
}