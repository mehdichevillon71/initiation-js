export class StockNegativeError extends Error
{
    constructor() {
        super("Stock cannot be negative and must be numeric")
    }
}