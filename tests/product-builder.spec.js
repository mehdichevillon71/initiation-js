const { Builder } = require("../_helpers/builder.class")
const { IdIsEmptyError } = require("../errors/id-is-empty.error")
const { NameIsEmptyError } = require("../errors/name-is-empty.error")
const { StockNegativeError } = require("../errors/stock-negative.error")
const { StockNullError } = require("../errors/stock-not-valid.error")
const { ProductBuilder } = require("../products/product-builder.class")
const { Product } = require("../products/product.class")

describe(`ProductBuilder`, () => {
    it(`Should be instanciated and to be an instance of Builder too`, () => {
        const productBuilder = new ProductBuilder()
        expect(productBuilder).toBeInstanceOf(ProductBuilder)
        expect(productBuilder).toBeInstanceOf(Builder)
    })

    it(`Should raised an exception if no ID`, () => {
        const productBuilder = new ProductBuilder()
        productBuilder.name = 'Test'
        productBuilder.stock = 10

        expect(() => productBuilder.build()).toThrow(IdIsEmptyError)
    })

    it(`Should raised an exception if no Name`, () => {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.stock = 10

        expect(() => productBuilder.build()).toThrow(NameIsEmptyError)
    })

    it(`Should raised an exception if no Numeric`, () => {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.name = 'Test'
        productBuilder.stock = 'Test'

        expect(() => productBuilder.build()).toThrow(StockNullError)
    })

    it(`Should raised an exception if numeric under 0`, () => {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.name = 'Test'
        productBuilder.stock = -5

        expect(() => productBuilder.build()).toThrow(StockNegativeError)
    })


    it(`Should give back a Product instance with correct values`, () => {
        const productBuilder = new ProductBuilder()
        productBuilder.id = 'Test'
        productBuilder.name = 'Test'
        productBuilder.stock = 10

        const product = productBuilder.build()

        expect(product).toBeInstanceOf(Product)
        expect(product.id).toEqual('Test')
        expect(product.name).toEqual('Test')
        expect(product.stock).toEqual(10)
    })
})