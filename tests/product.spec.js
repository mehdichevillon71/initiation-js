import { Builder } from "../_helpers/builder.class";
import { ProductBuilder } from "../products/product-builder.class";
import { Product } from "../products/product.class";

describe(`Product class`, () => {
    it(`Should make an instance of Product,`, () => {
        const product = Product.getInstance()
        expect(product).toBeInstanceOf(Product)
    })

    it(`Should give and id, a name and a stock to the objects class`, () => {

        const product = Product.getInstance()

        product.id = '1fe24'
        product.name = 'test'
        product.stock = 10

        expect(product.id).toEqual('1fe24')
        expect(product.name).toEqual('test')
        expect(product.stock).toEqual(10)

    })

})