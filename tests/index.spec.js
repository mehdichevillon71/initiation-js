const {primitiveAffectation, myBirthData, birtdata, objectCopy, usingPrototypePattern,usingArrays} = require("../index.js");

describe("index.js", () => {
    //Description Test suivis de l'algorithme test

  it('Should return "Remi', () => {
    expect(primitiveAffectation()).toBe("Remi")
  });

    it("Should have the same reference", () => {
        expect(birtdata).toStrictEqual(myBirthData);
    });

    it(`Should have different reference`, () =>
    {
        const theDate = new Date(1999, 12, 3)
        const otherDate = objectCopy(theDate)

        expect (theDate === otherDate).toBeFalsy()

        const protoDate = usingPrototypePattern(theDate)

        expect (theDate === protoDate).toBeFalsy()
    })

    it(`Should throw error if different`, () =>
    {
            expect(() => usingPrototypePattern('Wtf is that')).toThrow(TypeError);
    })
});